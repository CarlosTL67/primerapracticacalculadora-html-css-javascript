class Calculator{
    constructor(){
        this.pantalla = document.getElementById('pantalla_label')
        this.botones = Array.from(document.getElementById('botones').children)
        this.num1 = ""
        this.num2 = ""
        this.operador = ""
        this.contOperador=0
        this.resultado=""
        this.registrarEventos();

}

    registrarEventos(){
        this.botones.forEach(button=>{
        button.addEventListener('click',()=>this.procesarClick(button.innerHTML))
        })
    }

    procesarClick(boton){
    if(boton === 'Ac'){
        this.num1=""
        this.num2=""
        this.operador=""
        this.resultado=""
        this.pantalla.innerHTML=""
    }else if ((boton==='+' || boton==='-' || boton==='*' || boton==='/' || boton==='=') && this.num1!="") {
                switch (boton) {
                    case '+':
                        this.operador='+'
                        break;
                    case '-':
                        this.operador='-'
                        break;
                    case '*':
                        this.operador='*'
                        break;
                    case '/':
                        this.operador='/'
                        break;
                    case '=':
                        this.resultado =eval(parseInt(this.num1)+this.operador+parseInt(this.num2))
                        this.pantalla.innerHTML=this.resultado;
                        this.num2=""
                        this.num1=""
                        this.operador=""
                        this.pantalla.innerHTML=this.resultado;
                        break;
                    default:
                        break;
                }
        }
        if(!this.operador && boton !== 'Ac' && boton !=='='){
            this.num1+=boton
            this.pantalla.innerHTML=this.num1
    }if (/[0-9]/.test(boton) && (this.operador)) {
            this.num2+=boton
            this.pantalla.innerHTML=this.num2
    }
    }
}
const calculator = new Calculator();